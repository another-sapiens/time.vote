//! The API which handles poll-related database-interaction.

use super::super::DbConn;

use crate::errors::ApiResult;
use crate::helpers::{get_random_id, get_unique_id, validate_poll_admin};
use crate::models::{
    db_models::*,
    helper_models::*,
    schema::{administration as admn, polls, users, votes},
};

use diesel::prelude::*;
use rocket::{delete, get, patch, post, serde::json::Json};
use rocket_dyn_templates::Template;
use rocket_okapi::openapi;
use rocket_sync_db_pools::diesel::insert_into;
use validator::Validate;

/// Creates a new [`Poll`] using the supplied [`SetupData`]. Returns the link that should be navigated to next.
#[openapi(tag = "Poll")]
#[post("/create", format = "json", data = "<setup_data>", rank = 3)]
pub(crate) async fn new_poll(conn: DbConn, setup_data: Json<SetupData>) -> ApiResult<String> {
    // Get values
    let setup_data = setup_data.into_inner();
    setup_data.validate()?;

    let admin_email = setup_data.admin_email.clone();

    let (poll_id, admin_id): (String, String) = conn
        .run(
            |c: &mut diesel::PgConnection| -> ApiResult<(String, String)> {
                let poll_id_is_unique = |id: &str| -> ApiResult<bool> {
                    let is_unique = polls::table.find(id).execute(c)? == 0;
                    Ok(is_unique)
                };
                let poll_id = get_unique_id(poll_id_is_unique)?;
                let poll = Poll::new(&poll_id, setup_data);

                let admin_id: String = get_random_id(); // No need to check for uniqueness as it's the first user.
                let admin_user = User::new_admin(&poll.poll_id, &admin_id, admin_email);
                let administration_data = Administration::new(&poll_id, &admin_id);

                // Save to database
                let _ = insert_into(polls::table).values(poll).execute(c)?;
                let _ = insert_into(users::table).values(admin_user).execute(c)?;
                let _ = insert_into(admn::table)
                    .values(&administration_data)
                    .execute(c)?;

                Ok((poll_id, admin_id))
            },
        )
        .await?;

    let link = uri!(get_links(poll_id, admin_id)).to_string();
    Ok(link)
}

/// Returns site that displays the poll's access-links.
#[openapi(tag = "Poll")]
#[get("/<poll_id>/links/<admin_id>", rank = 3)]
pub(crate) async fn get_links(
    conn: DbConn,
    poll_id: String,
    admin_id: String,
) -> ApiResult<Template> {
    let poll = conn
        .run(move |c| -> ApiResult<Poll> {
            validate_poll_admin(&poll_id, &admin_id, c)?;

            let poll: Poll = polls::table.find(poll_id).first(c)?;
            Ok(poll)
        })
        .await?;

    Ok(Template::render("admin-specific/links", &poll))
}

/// Called to get the necessary information to let someone vote.
/// Returns a serialized [`Poll`].
#[openapi(tag = "Poll")]
#[get("/<poll_id>", rank = 3)]
pub(crate) async fn get_poll(conn: DbConn, poll_id: String) -> ApiResult<Json<Poll>> {
    conn.run(move |c| -> ApiResult<Poll> {
        let poll = polls::table.find(poll_id).first(c)?;

        Ok(poll)
    })
    .await
    .map(Json)
}

/// Returns [`SetupData`]. Used to let the [Poll]'s admin edit the metadata.
#[openapi(tag = "Poll")]
#[get("/<poll_id>/edit/<admin_id>", rank = 3)]
pub(crate) async fn get_poll_setupdata(
    conn: DbConn,
    poll_id: String,
    admin_id: String,
) -> ApiResult<Json<SetupData>> {
    conn.run(move |c| {
        validate_poll_admin(&poll_id, &admin_id, c)?;

        let poll: Poll = polls::table.find(&poll_id).first(c)?;
        let admin_email = users::table
            .find((&poll_id, &admin_id))
            .select(users::email)
            .first(c)?;

        Ok(SetupData::from_poll(&poll, &admin_email))
    })
    .await
    .map(Json)
}

/// Updates a [Poll]'s metadata according to the supplied values inn [`SetupData`].
#[openapi(tag = "Poll")]
#[patch(
    "/<poll_id>/edit/<admin_id>",
    format = "json",
    data = "<setup_data>",
    rank = 2
)]
pub(crate) async fn update_poll(
    conn: DbConn,
    poll_id: String,
    admin_id: String,
    setup_data: Json<SetupData>,
) -> ApiResult<()> {
    let setup_data = setup_data.into_inner();
    setup_data.validate()?;

    let admin_email = setup_data.admin_email.clone();
    let poll = Poll::new(&poll_id, setup_data);

    conn.run(move |c| -> ApiResult<()> {
        validate_poll_admin(&poll_id, &admin_id, c)?;

        // Save changes to Poll
        poll.save_changes::<Poll>(c)?;
        // Save changes to the admin's email-address
        let mut admin: User = users::table.find((&poll_id, &admin_id)).first(c)?;
        admin.email = admin_email;
        admin.save_changes::<User>(c)?;

        Ok(())
    })
    .await
}

/// Called to insert a user's [`Vote`].
#[openapi(tag = "Vote")]
#[post("/<poll_id>", format = "json", data = "<json_uservote>", rank = 4)]
pub(crate) async fn new_vote(
    conn: DbConn,
    poll_id: String,
    json_uservote: Json<BareUserVote>,
) -> ApiResult<String> {
    let uservote = json_uservote.into_inner();
    uservote.validate()?;

    conn.run(move |c: &mut diesel::PgConnection| {
        // Create a new User with an unique ID
        let user_id_is_unique = |id: &str| -> ApiResult<bool> {
            let is_unique = users::table.find((&poll_id, id)).execute(c)? == 0;
            Ok(is_unique)
        };
        let user_id = get_unique_id(user_id_is_unique)?;
        let user = User::new(&poll_id, &user_id, &uservote);

        // Create a new Vote with an unique ID
        let vote_id_is_unique = |id: &str| -> ApiResult<bool> {
            let is_unique = votes::table.find((&poll_id, id)).execute(c)? == 0;
            Ok(is_unique)
        };
        let vote_id = get_unique_id(vote_id_is_unique)?;
        let vote = Vote::new(&poll_id, &vote_id, &user_id, &uservote);

        // Insert User & Vote
        insert_into(users::table).values(user).execute(c)?;
        insert_into(votes::table).values(vote).execute(c)?;

        Ok(vote_id)
    })
    .await
}

/// Returns the [`Vote`] which corresponds to the supplied [vote_id].
#[openapi(tag = "Vote")]
#[get("/<poll_id>/update/<vote_id>", rank = 3)]
pub(crate) async fn get_vote(
    conn: DbConn,
    poll_id: String,
    vote_id: String,
) -> ApiResult<Json<BareUserVote>> {
    conn.run(
        move |c: &mut diesel::PgConnection| -> ApiResult<BareUserVote> {
            let vote: Vote = votes::table.find((&poll_id, &vote_id)).first(c)?;
            let user: User = users::table.find((&poll_id, &vote.voter_id)).first(c)?;

            Ok(BareUserVote::from_user_and_vote(&user, &vote))
        },
    )
    .await
    .map(Json)
}

/// Updates the [`Vote`] with the supplied vote_id.
#[openapi(tag = "Vote")]
#[patch(
    "/<poll_id>/update/<vote_id>",
    format = "json",
    data = "<uservote>",
    rank = 2
)]
pub(crate) async fn update_uservote(
    conn: DbConn,
    poll_id: String,
    vote_id: String,
    uservote: Json<BareUserVote>,
) -> ApiResult<()> {
    let new_uservote = uservote.into_inner();
    new_uservote.validate()?;

    conn.run(move |c: &mut diesel::PgConnection| -> ApiResult<()> {
        let mut vote: Vote = votes::table.find((&poll_id, &vote_id)).first(c)?;
        let mut user: User = users::table.find((&poll_id, &vote.voter_id)).first(c)?;

        // Save changes to the vote
        vote.comment = new_uservote.comment;
        vote.availability = new_uservote.availability;
        vote.save_changes::<Vote>(c)?;
        // Save changes to the user's data
        user.name = new_uservote.name;
        user.email = new_uservote.email;
        user.save_changes::<User>(c)?;

        Ok(())
    })
    .await
}

/// Deletes a the poll that corresponds to the supplied poll_id.
#[openapi(tag = "Vote")]
#[delete("/<poll_id>/update/<vote_id>", rank = 3)]
pub(crate) async fn delete_vote(conn: DbConn, poll_id: String, vote_id: String) -> ApiResult<()> {
    conn.run(move |c| -> ApiResult<()> {
        // Delete the vote
        let deleted_vote: Vote =
            diesel::delete(votes::table.find((&poll_id, &vote_id))).get_result(c)?;

        // Delete the user which corresponds to the vote IF the user is not the admin
        let admin_id: String = admn::table.find(&poll_id).select(admn::admin_id).first(c)?;
        if deleted_vote.voter_id != admin_id {
            diesel::delete(users::table.find((&poll_id, deleted_vote.voter_id))).execute(c)?;
        }

        Ok(())
    })
    .await
}

/// Returns all poll-data.
#[openapi(tag = "Poll")]
#[get("/<poll_id>/results/<admin_id>", rank = 3)]
pub(crate) async fn results(
    conn: DbConn,
    poll_id: String,
    admin_id: String,
) -> ApiResult<Json<PollResults>> {
    conn.run(move |c| -> ApiResult<PollResults> {
        validate_poll_admin(&poll_id, &admin_id, c)?;

        // Get poll with the desired `poll_id`
        let poll: Poll = polls::table.find(&poll_id).first(c)?;
        // Get corresponding votes & users
        let votes_with_users: Vec<(Vote, User)> = votes::table
            .filter(votes::poll_id.eq(&poll_id))
            .inner_join(
                users::table.on(votes::poll_id
                    .eq(users::poll_id)
                    .and(votes::voter_id.eq(users::user_id))),
            )
            .load(c)?;
        // Combine votes & users into `BareUserVote`s
        let uservotes: Vec<BareUserVote> = votes_with_users
            .into_iter()
            .map(|(vote, user)| BareUserVote::from_user_and_vote(&user, &vote).without_email())
            .collect();

        Ok(PollResults { poll, uservotes })
    })
    .await
    .map(Json)
}

/// Deletes a the poll that corresponds to the supplied poll_id.
#[openapi(tag = "Poll")]
#[delete("/<poll_id>/delete/<admin_id>", rank = 3)]
pub(crate) async fn delete_poll(conn: DbConn, poll_id: String, admin_id: String) -> ApiResult<()> {
    conn.run(move |c| -> ApiResult<()> {
        validate_poll_admin(&poll_id, &admin_id, c)?;

        // Delete all votes that belong to this poll
        diesel::delete(votes::table.filter(votes::poll_id.eq(&poll_id))).execute(c)?;
        // Delete the administration-data that belongs to this poll
        diesel::delete(admn::table.find(&poll_id)).execute(c)?;
        // Delete all users that participated in this poll
        diesel::delete(users::table.filter(users::poll_id.eq(&poll_id))).execute(c)?;
        // Delete the poll
        diesel::delete(polls::table.find(&poll_id)).execute(c)?;

        Ok(())
    })
    .await
}
