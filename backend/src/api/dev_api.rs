//! API used during development. Disable this when deploying the web-server.

use super::super::DbConn;

use crate::errors::{ApiError, ApiResult};
use crate::models::{
    db_models::*,
    helper_models::*,
    schema::{administration as admn, polls, users, votes},
};

use diesel::prelude::*;
use rocket::{get, serde::json::Json};
use rocket_okapi::openapi;

/// Does nothing but successfully returning some text.
#[openapi(tag = "Dev")]
#[get("/dev/test")]
pub fn test() -> &'static str {
    "Hello, test!"
}

/// Always returns an [`ApiError::Text()`].
#[openapi(tag = "Dev")]
#[get("/dev/error")]
pub fn error() -> ApiError {
    ApiError::Text("Use this to test errors.".to_string())
}

/// Returns everything currently stored in the database.
#[openapi(tag = "Dev")]
#[get("/dev/get_db")]
pub(crate) async fn get_db(conn: DbConn) -> ApiResult<Json<Vec<FullPollData>>> {
    conn.run(
        |c: &mut diesel::PgConnection| -> ApiResult<Vec<FullPollData>> {
            let polls: Vec<Poll> = polls::table.get_results(c)?;
            let mut results: Vec<FullPollData> = Vec::with_capacity(polls.len());

            for poll in polls {
                let result_admn: Administration = admn::table.find(&poll.poll_id).first(c)?;
                let result_votes: Vec<Vote> = votes::table
                    .filter(votes::poll_id.eq(&poll.poll_id))
                    .get_results(c)?;
                let result_users: Vec<User> = users::table
                    .filter(users::poll_id.eq(&poll.poll_id))
                    .get_results(c)?;

                results.push(FullPollData {
                    poll,
                    administration: result_admn,
                    users: result_users,
                    votes: result_votes,
                });
            }

            Ok(results)
        },
    )
    .await
    .map(Json)
}
