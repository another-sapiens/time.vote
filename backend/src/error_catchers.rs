//! Defines tha catchers for all possible error-codes.

/// Displays some kind of "This Poll does not exist" message in case of most errors.
#[catch(default)]
fn default() -> &'static str {
    "This Poll doesn't exist"
}

/// Used when the wrong admin_id was used in combination with a poll.
#[catch(403)]
fn forbidden() -> &'static str {
    "This Poll exists, but your admin-ID is incorrect."
}

/// Used when there is some internal error.
#[catch(500)]
fn internal() -> &'static str {
    "Something went wrong inside the server."
}

/// Returns all catchers in [self].
pub fn catchers() -> Vec<rocket::Catcher> {
    catchers![default, forbidden, internal]
}
