#[macro_use]
extern crate rocket;
#[macro_use]
extern crate diesel;

use rocket_dyn_templates::Template;
use rocket_okapi::{
    gen::OpenApiGenerator,
    request::{OpenApiFromRequest, RequestHeaderInput},
    swagger_ui::{make_swagger_ui, SwaggerUIConfig},
};
use rocket_sync_db_pools::database;

mod api;
mod error_catchers;
mod errors;
mod helpers;
mod models;

/// Connection to the server's [diesel]-PostgreSQL-database.
#[database("time_poll")]
struct DbConn(diesel::PgConnection);

impl<'r> OpenApiFromRequest<'r> for DbConn {
    /// Implementation required by OkApi. See [Here](https://github.com/GREsau/okapi#faq).
    fn from_request_input(
        _gen: &mut OpenApiGenerator,
        _name: String,
        _required: bool,
    ) -> rocket_okapi::Result<RequestHeaderInput> {
        Ok(RequestHeaderInput::None)
    }
}
fn get_docs() -> SwaggerUIConfig {
    // use rocket_okapi::settings::UrlObject;

    SwaggerUIConfig {
        url: "/openapi.json".to_string(),
        ..Default::default()
    }
}

/// The analog to the usual `fn main()`.
#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/", api::routes())
        .register("/", error_catchers::catchers())
        .mount("/swagger", make_swagger_ui(&get_docs()))
        .attach(DbConn::fairing())
        .attach(Template::fairing())
}
