-- Your SQL goes here

CREATE TABLE polls (
  poll_id VARCHAR(7) NOT NULL,
  title VARCHAR(100) NOT NULL,
  instruction VARCHAR(1000) NOT NULL,
  datetime_interval SMALLINT NOT NULL,
  offered_datetimes TIMESTAMPTZ(0)[50] NOT NULL,
  deadline TIMESTAMPTZ(0) NOT NULL,
  theme VARCHAR(25) NOT NULL,
  
  PRIMARY KEY (poll_id)
);

-- May also be called "participants"
CREATE TABLE users (
  poll_id VARCHAR(7) NOT NULL,
  user_id VARCHAR(7) NOT NULL,
  name VARCHAR(100) NOT NULL,
  email VARCHAR(250),

  PRIMARY KEY (poll_id, user_id),
  FOREIGN KEY (poll_id) REFERENCES polls(poll_id)
);

CREATE TABLE votes (
  poll_id VARCHAR(7) NOT NULL,
  vote_id VARCHAR(7) NOT NULL,
  voter_id VARCHAR(7) NOT NULL,
  comment VARCHAR(1000),
  availability TIMESTAMPTZ(0)[250] NOT NULL,

  PRIMARY KEY (poll_id, vote_id),
  FOREIGN KEY (poll_id) REFERENCES polls(poll_id),
  FOREIGN KEY (poll_id, voter_id) REFERENCES users(poll_id, user_id)
);

CREATE TABLE administration (
  poll_id VARCHAR(7) NOT NULL,
  admin_id VARCHAR(7) NOT NULL,
  creation_date TIMESTAMPTZ(0) NOT NULL,

  PRIMARY KEY (poll_id),
  FOREIGN KEY (poll_id) REFERENCES polls(poll_id),
  FOREIGN KEY (poll_id, admin_id) REFERENCES users(poll_id, user_id)
);
