# Project Moved!

1. Development is continued in [this repository](https://gitlab.com/Glitchy-Tozier/time.vote)
2. The project now is live on [voteon.date](https://voteon.date/)

---

# time.vote

## Install Rust & cargo (one time only)
See https://www.rust-lang.org/learn/get-started

## Setting up the database (one time only)
### 1. Install [postgresql](https://www.postgresql.org/download/linux/)
Make sure to double-check whether all packages actually have been installed. Example from [postgres-ubuntu-instructions](https://www.postgresql.org/download/linux/ubuntu/):
![](postgres_install.png)

### 2. Additionally, make sure user "postgres" has password "postgres":
```sh
sudo -u postgres psql
    \# ALTER USER postgres PASSWORD postgres;
```

### 3. Set up database
All of the following commands use [diesel_cli](https://diesel.rs/guides/getting-started)
```sh
cargo install diesel_cli --no-default-features --features postgres
cd backend # Move into backend directory
diesel setup
cd .. # Go back into the root directory
```

## Update database
If database-changes have taken place, run the following command to re-create the database (this will delete the data in the previous database):
```sh
cd backend # Move into backend directory
diesel migration redo
cd .. # Go back into the root directory
```

## Prepare frontend
First, move into the `frontend` directory:
```sh
cd frontend
```
The frontend could be tested on its own using `npm run dev` but since the backend will serve it, we're gonna build it:
```sh
npm run build
```
Lastly, go back into the root directory:
```sh
cd ..
```

## Start server
1. Move into the `backend` directory
2. Start the server
3. When the server is stopped, return to the root directory

```sh
cd backend && cargo run && cd ..
```

## API-docs
Once the server is running, visit `127.0.0.1:8000/swagger/`.
