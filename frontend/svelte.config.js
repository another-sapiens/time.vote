import adapter from "@sveltejs/adapter-static";
import preprocess from "svelte-preprocess";
import path from "path";

/** @type {import('@sveltejs/kit').Config} */
const config = {
    // Consult https://github.com/sveltejs/svelte-preprocess
    // for more information about preprocessors
    preprocess: preprocess(),

    kit: {
        adapter: adapter({
            pages: "dist",
            assets: "dist",
            precompress: false,
        }),
        prerender: {
            default: true,
        },
        vite: {
            resolve: {
                alias: {
                    $components: path.resolve("./src/components"),
                    $stores: path.resolve("./src/stores"),
                },
            },
        },
        browser: {
            // https://kit.svelte.dev/docs/page-options#router
            // https://kit.svelte.dev/docs/configuration#browser
            router: false,
        }
    },
};

export default config;
