#!/bin/bash

#####################      Variables     #############################

SOURCE_DIR="dist/poll-specific"


########### Do not edit below this until required  #################

### Test if source directory exists
### The script will stop if source does not exists

if [ -d "${SOURCE_DIR}" ]; then
        echo "Source directory found"
else
        echo "Source directory not found. Please check above variables are set correctly"
        echo "script exited"
        exit 1
fi

### Go to source directory

cd "${SOURCE_DIR}"

### Create copies of files in the root of our source-directory,
### adding the file-extension ".tera" to each of the copies

for file in *.html
do
        cp "${file}" "${file}.tera"
done

### Do the same thing for each of the sub-directories.

for dir in */
do
        cd "${dir}"
        for file in *.html
        do
                cp "${file}" "${file}.tera"
        done
        cd ".."
done

### Print success-message

echo "Created template-files. They are the exact same files, just with the \`.tera\`-extension added."

###################  End of Script  ###################################
