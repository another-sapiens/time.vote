These pages can be navigated to by calling `time.poll/{poll-id}/{action}/{vote-id}`.

Keep in mind that there are endpoints at the server that might not show up in this file-structure.
An example might be deleting a vote.
